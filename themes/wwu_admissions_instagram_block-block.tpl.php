<?php

/**
 * @file
 * WWU Admissions Instagram Block block template.
 *
 * Available variables:
 * - $images: Array of images to be rendered.
 */
?>
<div class="wwu-admissions-instagram-block">
<?php if ($account): ?>
  <div class="wwu-admissions-instagram-account">
  <?php print render($account); ?>
  </div>
<?php endif; ?>
  <div class="wwu-admissions-instagram-images">
  <?php foreach ($images as $image): ?>
    <?php print render($image); ?>
  <?php endforeach; ?>
  </div>
</div>
