<?php

/**
 * @file
 * WWU Admissions Instagram Block image template.
 *
 * Available variables:
 * - $post: Full post object.
 * - $href: The link to the originating Instagram post.
 * - $src: The URL to the image served by the Instagram post.
 */
?>
<div class="wwu-admissions-instagram-image">
  <a href="<?php print $href; ?>" target="_blank">
    <img src="<?php print $src; ?>" />
  </a>
</div>
